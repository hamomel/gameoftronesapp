package com.hamom.romanzotov.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamom.romanzotov.R;

import com.hamom.romanzotov.ui.adapters.MainRecyclerAdapter;
import com.hamom.romanzotov.utils.ConstantManager;
import com.hamom.romanzotov.utils.GotApplication;



public class MainFragment extends Fragment {
    private static final String TAG = ConstantManager.TAG_PREFIX + "Fragment ";

    public static final String ARG_OBJECT = "object";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_main, container, false);




        LinearLayoutManager manager = new LinearLayoutManager(GotApplication.getContext());
        ((RecyclerView) view.findViewById(R.id.characters_list)).setLayoutManager(manager);

        MainRecyclerAdapter adapter = new MainRecyclerAdapter(getArguments().getInt(ARG_OBJECT));
        Log.d(TAG, "onCreateView: " + getArguments().getInt(ARG_OBJECT));

        ((RecyclerView) view.findViewById(R.id.characters_list)).setAdapter(adapter);

        return view;
    }


}
