package com.hamom.romanzotov.ui;

import android.app.ActionBar;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hamom.romanzotov.R;

import com.hamom.romanzotov.mvp.presenters.CharacterDetailPresenter;
import com.hamom.romanzotov.mvp.views.ICharacterDetailView;
import com.hamom.romanzotov.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterDetailActivity extends AppCompatActivity implements ICharacterDetailView, View.OnClickListener{
    private static final String TAG = ConstantManager.TAG_PREFIX + "CharActivity ";

    @BindView(R.id.toolbar_layout) CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.toolbar_character) Toolbar mToolbar;
    @BindView(R.id.words) TextView mWords;
    @BindView(R.id.born) TextView mBorn;
    @BindView(R.id.titles) TextView mTitles;
    @BindView(R.id.aliases) TextView mAliases;
    @BindView(R.id.father) TextView mFather;
    @BindView(R.id.mother) TextView mMother;
    @BindView(R.id.toolbar_image) ImageView mImageView;
    @BindView(R.id.coordinator_layout) CoordinatorLayout mCoordinatorLayout;

    private static final String CHARACTER_URL = "character_url";
    private CharacterDetailPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_detail);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mPresenter = CharacterDetailPresenter.getInstance();

        if (savedInstanceState == null){
            String url = getIntent().getStringExtra(ConstantManager.URL_KEY);
            mPresenter.takeView(this, url);
            mPresenter.initView();
        } else {

            mPresenter.takeView(this);
            mPresenter.initView();
        }





        mFather.setOnClickListener(this);
        mMother.setOnClickListener(this);



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

    }



    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home)
        finish();
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: " + v.getId());
        switch (v.getId()){
            case R.id.father:
              mPresenter.onFatherClick();
                break;
            case R.id.mother:
                mPresenter.onMotherClick();
                break;

        }

    }

    @Override
    public void setNameText(String text) {
        mCollapsingToolbarLayout.setTitle(text);

    }

    @Override
    public void setWords(String text) {
        mWords.setText(text);
    }

    @Override
    public void setBornText(String text) {
        mBorn.setText(text);
    }

    @Override
    public void setDiedText(String text) {

    }

    @Override
    public void setTitlesText(String text) {
        mTitles.setText(text);
    }

    @Override
    public void setAliasesText(String text) {
        mAliases.setText(text);
    }

    @Override
    public void setFatherText(String text) {
        mFather.setText(text);
    }

    @Override
    public void setMOtherText(String text) {
        mMother.setText(text);
    }

    @Override
    public void setImage(int res) {
        Picasso.with(this).load(res).fit().into(mImageView);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }
}
