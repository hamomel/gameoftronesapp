package com.hamom.romanzotov.ui.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.hamom.romanzotov.ui.fragments.MainFragment;
import com.hamom.romanzotov.utils.ConstantManager;




public class MainPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = ConstantManager.TAG_PREFIX + "PagerAdapter ";

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(MainFragment.ARG_OBJECT, position);

        fragment.setArguments(args);
        Log.d(TAG, "getItem: " + fragment.getArguments().getParcelableArrayList(MainFragment.ARG_OBJECT));
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }



}
