package com.hamom.romanzotov.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.mvp.presenters.StartPresenter;
import com.hamom.romanzotov.mvp.views.IStartView;
import com.hamom.romanzotov.utils.ConstantManager;

public class StartActivity extends AppCompatActivity implements IStartView {
    private static final String TAG = ConstantManager.TAG_PREFIX + "StartActivity ";

    private StartPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mPresenter = new StartPresenter();
        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    public void startNewActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
