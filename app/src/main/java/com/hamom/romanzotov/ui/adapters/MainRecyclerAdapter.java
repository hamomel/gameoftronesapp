package com.hamom.romanzotov.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.mvp.presenters.RecyclerPresenter;
import com.hamom.romanzotov.mvp.views.MvpViewHolder;
import com.hamom.romanzotov.utils.ConstantManager;




public class MainRecyclerAdapter extends RecyclerView.Adapter<MvpViewHolder> {

    private static final String TAG = ConstantManager.TAG_PREFIX + "RecAdapter ";
    private RecyclerPresenter mPresenter;
    private int mItemCount;
    private int mPosition;

    public MainRecyclerAdapter(int position){
        mPosition = position;
        mPresenter = new RecyclerPresenter();
        mPresenter.takeView(this);
        mPresenter.setItemCount(position);
        Log.d(TAG, "MainRecyclerAdapter: " + position);
    }

    public void setItemCount(int count) {
        mItemCount = count;
    }



    @Override
    public MvpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_main, parent, false);
        return new MvpViewHolder(view, mPosition);
    }

    @Override
    public void onBindViewHolder(MvpViewHolder holder, int position) {

        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return mItemCount;
    }

    @Override
    public void onViewRecycled(MvpViewHolder holder) {
        super.onViewRecycled(holder);
        holder.unbindPresenter();
    }


}
