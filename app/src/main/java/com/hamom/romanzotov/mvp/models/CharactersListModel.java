package com.hamom.romanzotov.mvp.models;

import com.hamom.romanzotov.data.managers.DataManager;
import com.hamom.romanzotov.data.models.CharacterModel;

import java.util.List;

/**
 * Created by hamom on 24.10.16.
 */

public  class CharactersListModel implements ICharactersListModel {
    private static CharactersListModel instance;
    private List<List<CharacterModel>> mLists;
    private DataManager mDataManager = DataManager.getINSTANCE();

    public CharactersListModel() {
        mLists = mDataManager.getCharactersList();
    }

    public static CharactersListModel getInstance(){
        if (instance == null) instance = new CharactersListModel();
        return instance;
    }


    @Override
    public int getListsQuantity() {
        return mLists.size();
    }

    @Override
    public int getListSizeAt(int position) {
        return mLists.get(position).size();
    }

    @Override
    public CharacterModel getCharacter(int listNumber, int position) {
        return mLists.get(listNumber).get(position);
    }

    @Override
    public String getCharactersUrl(int listNumber, int position) {
        return mLists.get(listNumber).get(position).getUrl();
    }

    @Override
    public String getCharactersName(int listNumber, int position) {
        return mLists.get(listNumber).get(position).getName();
    }

    @Override
    public String getCharactersAllegiances(int listNumber, int position) {
        return mLists.get(listNumber).get(position).getAllegiance();
    }

    @Override
    public String getCharactersTitles(int listNumber, int position) {
        return mLists.get(listNumber).get(position).getTitles();
    }

    @Override
    public String getCharactersAliases(int listNumber, int position) {
        return mLists.get(listNumber).get(position).getAliases();
    }
}
