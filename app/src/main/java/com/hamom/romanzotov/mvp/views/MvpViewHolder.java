package com.hamom.romanzotov.mvp.views;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.mvp.presenters.ViewHolderPresenter;
import com.hamom.romanzotov.ui.CharacterDetailActivity;
import com.hamom.romanzotov.utils.ConstantManager;
import com.hamom.romanzotov.utils.GotApplication;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hamom on 25.10.16.
 */

public class MvpViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static String TAG = ConstantManager.TAG_PREFIX + "ViewHolder: ";
    public @BindView(R.id.icon_main)
    ImageView icon;

    public @BindView(R.id.name)
    TextView name;

    public @BindView(R.id.title)
    TextView title;

    private ViewHolderPresenter mPresenter;

    public MvpViewHolder(View itemView, int position) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        Log.d(TAG, "MvpViewHolder: " + position + getAdapterPosition());
        mPresenter = new ViewHolderPresenter(position);
        mPresenter.takeView(this);
        itemView.setOnClickListener(this);
    }

    public void bind(int position){
        mPresenter.initView(position);
    }

    @Override
    public void onClick(View v) {
        mPresenter.onViewClicked();

    }

    public void startActivity(String url){
        Intent intent = new Intent(GotApplication.getContext(), CharacterDetailActivity.class);
                intent.putExtra(ConstantManager.URL_KEY, url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                GotApplication.getContext().startActivity(intent);
    }

    public void unbindPresenter() {
//        mPresenter.dropView();
    }


}
