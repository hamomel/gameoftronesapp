package com.hamom.romanzotov.mvp.models;

import com.hamom.romanzotov.data.models.CharacterModel;

/**
 * Created by hamom on 24.10.16.
 */

public interface ICharactersListModel {

    int getListsQuantity();
    int getListSizeAt(int position);
    CharacterModel getCharacter(int listNumber, int position);

    String getCharactersUrl(int listNumber, int position);
    String getCharactersName(int listNumber, int position);
    String getCharactersAllegiances(int listNumber, int position);
    String getCharactersTitles(int listNumber, int position);
    String getCharactersAliases(int listNumber, int position);
}
