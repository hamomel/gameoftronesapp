package com.hamom.romanzotov.mvp.models;

import com.hamom.romanzotov.data.managers.DataManager;

public class StartModel {
    private DataManager mDataManager;

    public StartModel(){
        mDataManager = DataManager.getINSTANCE();
    }

    public void fillDb(){
        mDataManager.fillDb();
    }
}
