package com.hamom.romanzotov.mvp.models;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.data.managers.DataManager;
import com.hamom.romanzotov.data.models.CharacterModel;
import com.hamom.romanzotov.data.models.HouseModel;
import com.hamom.romanzotov.data.models.HouseModelDao;
import com.hamom.romanzotov.utils.ConstantManager;
import com.hamom.romanzotov.utils.GotApplication;
import com.squareup.picasso.Picasso;

/**
 * Created by hamom on 23.10.16.
 */

public class CharacterDetailModel implements ICharacterDetailModel {
    private DataManager mDataManager = DataManager.getINSTANCE();
    private CharacterModel mModel;



    @Override
    public void setCharacter(String url) {
        mModel = mDataManager.getCharacter(url);

    }

    @Override
    public String getCharactersName() {
        return mModel.getName();
    }

    @Nullable
    @Override
    public String getCharactersWords() {
        if (!TextUtils.isEmpty(mModel.getAllegiance())) {
            String words = mDataManager
                    .getDaoSession()
                    .queryBuilder(HouseModel.class)
                    .where(HouseModelDao.Properties.Url.eq(mModel.getAllegiance()))
                    .build()
                    .unique()
                    .getWords();
            return words;
        } else {
            return null;

        }
    }


    @Nullable
    @Override
    public String getCharactersBorn() {
        return mModel.getBorn();
    }

    @Nullable
    @Override
    public String getCharactersDied() {
        return mModel.getDied();
    }

    @Nullable
    @Override
    public String getCharactersTitles() {
        return mModel.getTitles();
    }

    @Nullable
    @Override
    public String getCharactersAliases() {
        return mModel.getAliases();
    }

    @Nullable
    @Override
    public String getCharactersMotherName() {
        if (!TextUtils.isEmpty(mModel.getFather())){
            return mDataManager.getCharacter(getCharactersMotherUrl()).getName();

        } else {
            return null;

        }
    }

    @Nullable
    @Override
    public String getCharactersMotherUrl() {
        return mModel.getMother();
    }

    @Nullable
    @Override
    public String getCharactersFatherName() {
        if (!TextUtils.isEmpty(mModel.getFather())){
           return mDataManager.getCharacter(getCharactersFatherUrl()).getName();

        } else {
            return null;

        }
    }

    @Nullable
    @Override
    public String getCharactersFatherUrl() {
        return mModel.getFather();
    }

    @Nullable
    @Override
    public int getCharactersImageId() {
        int id = 0;
        if (!TextUtils.isEmpty(mModel.getAllegiance())){
            switch (mModel.getAllegiance()){
                case ConstantManager.STARK_HOUSE_URL:

                    id = R.drawable.starks;
                    break;
                case ConstantManager.LANNISTER_HOUSE_URL:
                    id = R.drawable.lannister;

                    break;
                case ConstantManager.TARGARYEN_HOUSE_URL:
                    id = R.drawable.targarien;
                    break;

            }
        }
        return id;
    }

    @Nullable
    @Override
    public String getLastSeason() {
        return mModel.getLastSeason();
    }

    @Override
    public String getCharacterUrl() {
        return mModel.getUrl();
    }
}
