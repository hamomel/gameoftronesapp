package com.hamom.romanzotov.mvp.presenters;

import com.hamom.romanzotov.ui.CharacterDetailActivity;

/**
 * Created by hamom on 23.10.16.
 */

public interface ICharacterDetailPresenter<V> {

    void takeView(V view, String url);
    void dropView();
    void initView();

    V getView();

    void onFatherClick();
    void onMotherClick();

    void takeView(CharacterDetailActivity characterDetailActivity);
}
