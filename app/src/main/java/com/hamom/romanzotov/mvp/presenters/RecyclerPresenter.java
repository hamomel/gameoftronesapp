package com.hamom.romanzotov.mvp.presenters;

import com.hamom.romanzotov.mvp.models.CharactersListModel;
import com.hamom.romanzotov.mvp.models.ICharactersListModel;
import com.hamom.romanzotov.ui.adapters.MainRecyclerAdapter;

import java.lang.ref.WeakReference;

/**
 * Created by hamom on 24.10.16.
 */

public class RecyclerPresenter {
    private ICharactersListModel mModel;
    private WeakReference<MainRecyclerAdapter> mView;

    public RecyclerPresenter(){
        mModel = CharactersListModel.getInstance();
    }

    public void takeView(MainRecyclerAdapter view){
        mView = new WeakReference<MainRecyclerAdapter>(view);
    }

    private MainRecyclerAdapter getView(){
        return mView.get();
    }

    public void setItemCount(int position){
        getView().setItemCount(mModel.getListSizeAt(position));
    }
}
