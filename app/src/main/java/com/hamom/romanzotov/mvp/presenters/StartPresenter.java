package com.hamom.romanzotov.mvp.presenters;

import android.os.Handler;

import com.hamom.romanzotov.mvp.models.StartModel;
import com.hamom.romanzotov.mvp.views.IStartView;
import com.hamom.romanzotov.utils.ConstantManager;

import java.lang.ref.WeakReference;

public class StartPresenter {
    private StartModel mModel;
    private WeakReference<IStartView> mView;

    public StartPresenter() {
        mModel = new StartModel();
    }

    public void takeView(IStartView view) {
        mView = new WeakReference<>(view);
    }

    public IStartView getView(){
        return mView.get();
    }

    public void initView() {
        mModel.fillDb();
        changeActivity();
    }

    private void changeActivity(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getView().startNewActivity();
            }
        }, ConstantManager.ACTIVITY_START_DELAY);
    }
}
