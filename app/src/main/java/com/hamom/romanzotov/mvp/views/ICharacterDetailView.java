package com.hamom.romanzotov.mvp.views;

public interface ICharacterDetailView {

    void setNameText(String text);
    void setWords(String text);
    void setBornText(String text);
    void setDiedText(String text);
    void setTitlesText(String text);
    void setAliasesText(String text);
    void setFatherText(String text);
    void setMOtherText(String text);
    void setImage(int res);
    void showMessage(String message);
}
