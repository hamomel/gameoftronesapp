package com.hamom.romanzotov.mvp.presenters;

import android.text.TextUtils;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.mvp.models.CharactersListModel;
import com.hamom.romanzotov.mvp.views.MvpViewHolder;
import com.hamom.romanzotov.utils.ConstantManager;

import java.lang.ref.WeakReference;

/**
 * Created by hamom on 25.10.16.
 */
public class ViewHolderPresenter {
    private CharactersListModel mModel;
    private WeakReference<MvpViewHolder> mView;
    int mPosition;
    int mListNumber;

    public ViewHolderPresenter(int listNumber) {
        mModel = CharactersListModel.getInstance();
        mListNumber = listNumber;
    }

    public void takeView(MvpViewHolder view){
        mView = new WeakReference<>(view);
    }

    public void dropView(){
        mView = null;
    }

    public MvpViewHolder getView(){
        return mView.get();
    }

    public void initView(int position) {
        mPosition = position;

        switch (mModel.getCharactersAllegiances(mListNumber, position)){
            case ConstantManager.STARK_HOUSE_URL:
                getView().icon.setImageResource(R.drawable.stark_icon);
                break;
            case ConstantManager.LANNISTER_HOUSE_URL:
                getView().icon.setImageResource(R.drawable.lanister_icon);
                break;

            case ConstantManager.TARGARYEN_HOUSE_URL:
                getView().icon.setImageResource(R.drawable.targarien_icon);
                break;
        }

        getView().name.setText(mModel.getCharactersName(mListNumber, position));


        if (TextUtils.isEmpty(mModel.getCharactersTitles(mListNumber, position))){
            getView().title.setText(mModel.getCharactersAliases(mListNumber, position));

        } else {
            getView().title.setText(mModel.getCharactersTitles(mListNumber, position));
        }
    }

    public void onViewClicked(){

        getView().startActivity(mModel.getCharactersUrl(mListNumber, mPosition));
    }
}
