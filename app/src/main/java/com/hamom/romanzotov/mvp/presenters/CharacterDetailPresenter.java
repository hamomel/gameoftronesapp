package com.hamom.romanzotov.mvp.presenters;

import android.text.TextUtils;

import com.hamom.romanzotov.R;
import com.hamom.romanzotov.mvp.models.CharacterDetailModel;
import com.hamom.romanzotov.mvp.views.ICharacterDetailView;
import com.hamom.romanzotov.ui.CharacterDetailActivity;
import com.hamom.romanzotov.utils.GotApplication;

import java.lang.ref.WeakReference;

/**
 * Created by hamom on 23.10.16.
 */

public class CharacterDetailPresenter implements ICharacterDetailPresenter {
    private static CharacterDetailPresenter instance;
    private CharacterDetailModel mModel;
    private WeakReference<ICharacterDetailView> mView;

    private CharacterDetailPresenter() {
        mModel = new CharacterDetailModel();
    }

    public static CharacterDetailPresenter getInstance(){
        if (instance == null) instance = new CharacterDetailPresenter();
        return instance;
    }

    @Override
    public void takeView(Object view, String url) {
        mView = new WeakReference<>((ICharacterDetailView)view);
        setModel(url);
    }

    private void setModel(String url) {
        mModel = new CharacterDetailModel();
        mModel.setCharacter(url);
    }

    @Override
    public void dropView() {
        mView.clear();

    }

    @Override
    public void initView() {
        if(isSetupDone()){
            int imageId = mModel.getCharactersImageId();
            if (imageId != 0) getView().setImage(imageId);

            String name = mModel.getCharactersName();
            if (!TextUtils.isEmpty(name))getView().setNameText(name);

            String words = mModel.getCharactersWords();
            if (!TextUtils.isEmpty(words))getView().setWords(words);

            String born = mModel.getCharactersBorn();
            if (!TextUtils.isEmpty(born))getView().setBornText(born);

            String died = mModel.getCharactersDied();
            if (!TextUtils.isEmpty(died))getView().setDiedText(died);

            String titles = mModel.getCharactersTitles();
            if (!TextUtils.isEmpty(titles))getView().setTitlesText(titles);

            String aliases = mModel.getCharactersTitles();
            if (!TextUtils.isEmpty(aliases))getView().setAliasesText(aliases);

            String father = mModel.getCharactersFatherName();
            if (!TextUtils.isEmpty(father)){
                getView().setFatherText(father);
            }else {
                getView().setFatherText(GotApplication.getContext().getString(R.string.unknown));
            }

            String mother = mModel.getCharactersMotherName();
            if (!TextUtils.isEmpty(mother)){
                getView().setMOtherText(mother);
            } else {
                getView().setMOtherText(GotApplication.getContext().getString(R.string.unknown));
            }


            if (!TextUtils.isEmpty(died)){
                String season = mModel.getLastSeason();
                if (!TextUtils.isEmpty(season)){
                    getView().showMessage(GotApplication.getContext().getString(R.string.died_in) + season);
                }

            }
        }

    }

    private boolean isSetupDone() {
        return mModel != null && mView.get() != null;
    }

    public String getCharacterUrl(){
        return mModel.getCharacterUrl();
    }

    @Override
    public ICharacterDetailView getView() {
        return mView.get();
    }

    @Override
    public void onFatherClick() {
        String url = mModel.getCharactersFatherUrl();
        if (!TextUtils.isEmpty(url)){
            mModel.setCharacter(url);
            initView();
        }


    }

    @Override
    public void onMotherClick() {
        String url = mModel.getCharactersMotherUrl();
        if (!TextUtils.isEmpty(url)){
            mModel.setCharacter(url);
            initView();
        }

    }

    @Override
    public void takeView(CharacterDetailActivity characterDetailActivity) {
        mView = new WeakReference<>((ICharacterDetailView) characterDetailActivity);

    }
}
