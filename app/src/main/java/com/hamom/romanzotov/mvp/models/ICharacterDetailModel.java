package com.hamom.romanzotov.mvp.models;

import android.support.annotation.Nullable;

import com.hamom.romanzotov.data.models.CharacterModel;

/**
 * Created by hamom on 23.10.16.
 */

public interface ICharacterDetailModel {

    void setCharacter(String url);

    String getCharactersName();
    @Nullable
    String getCharactersWords();
    @Nullable
    String getCharactersBorn();
    @Nullable
    String getCharactersDied();
    @Nullable
    String getCharactersTitles();
    @Nullable
    String getCharactersAliases();
    @Nullable
    String getCharactersMotherName();
    @Nullable
    String getCharactersMotherUrl();
    @Nullable
    String getCharactersFatherName();
    @Nullable
    String getCharactersFatherUrl();
    @Nullable
    int getCharactersImageId();
    @Nullable
    String getLastSeason();


    String getCharacterUrl();
}
