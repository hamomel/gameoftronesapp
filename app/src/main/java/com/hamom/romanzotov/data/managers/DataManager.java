package com.hamom.romanzotov.data.managers;

import android.content.Context;
import android.util.Log;

import com.hamom.romanzotov.data.models.CharacterModel;
import com.hamom.romanzotov.data.models.CharacterModelDao;
import com.hamom.romanzotov.data.models.DaoSession;
import com.hamom.romanzotov.data.models.HouseModel;
import com.hamom.romanzotov.data.network.RestService;
import com.hamom.romanzotov.data.network.ServiceGenerator;
import com.hamom.romanzotov.data.network.res.CharacterRes;
import com.hamom.romanzotov.data.network.res.HouseRes;
import com.hamom.romanzotov.utils.ConstantManager;
import com.hamom.romanzotov.utils.GotApplication;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataManager {
    private static final String TAG = ConstantManager.TAG_PREFIX + "DataManager";

    private static DataManager INSTANCE;

    private Context mContext;
    private RestService mRestService;
    private DaoSession mDaoSession;
    private AsyncSession mAsyncSession;

    private DataManager() {
        mContext = GotApplication.getContext();
        mRestService = ServiceGenerator.createService(RestService.class);
        mDaoSession = GotApplication.getDaoSession();
    }

    public static DataManager getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }

    public RestService getRestService() {
        return mRestService;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public Call<HouseRes> loadHouse(String houseNo) {
        return mRestService.houseReq(houseNo);
    }

    public Call<CharacterRes> loadCharacter(String characterNo) {
        return mRestService.characterReq(characterNo);
    }

    public void fillDb() {
        String[] houses = {ConstantManager.LANNISTER_HOUSE_URL,
                ConstantManager.STARK_HOUSE_URL,
                ConstantManager.TARGARYEN_HOUSE_URL};

        mAsyncSession = mDaoSession.startAsyncSession();

        for (String house : houses) {
            Call<HouseRes> call = loadHouse(house.substring(house.lastIndexOf("/")));
            call.enqueue(new Callback<HouseRes>() {
                @Override
                public void onResponse(Call<HouseRes> call, Response<HouseRes> response) {
                    if (response.code() == 200) {
                        saveHouseInDb(response);
                        loadMembersFromApi(response);
                    } else {
                        Log.d(TAG, "onHouseResponse: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<HouseRes> call, Throwable t) {

                }
            });
        }
    }

    private void saveHouseInDb(Response<HouseRes> response) {
        HouseModel model = new HouseModel();
        model.setUrl(response.body().getUrl());
        model.setWords(response.body().getWords());

        Log.d(TAG, "saveHouseInDb: " + model.getUrl() + " " + model.getWords());

        mAsyncSession.insertOrReplaceInTx(HouseModel.class, model);
    }

    private void loadMembersFromApi(Response<HouseRes> response) {
        List<String> charUrls = response.body().getSwornMembers();
        final String houseUrl = response.body().getUrl();

        for (String charUrl : charUrls) {
            loadOneCharacter(charUrl, houseUrl);
        }
    }

    private void loadOneCharacter(String charUrl, final String houseUrl) {
        String charNo = charUrl.substring(charUrl.lastIndexOf("/"));
        Call<CharacterRes> call = loadCharacter(charNo);
        call.enqueue(new Callback<CharacterRes>() {
            @Override
            public void onResponse(Call<CharacterRes> call, Response<CharacterRes> response) {
                if (response.code() == 200) {
                    addNewCharacter(response, houseUrl);
                    if (!response.body().getFather().isEmpty()) {
                        loadOneCharacter(response.body().getFather(), "");
                    }

                    if (!response.body().getMother().isEmpty()) {
                        loadOneCharacter(response.body().getMother(), "");
                    }
                } else {
                    Log.d(TAG, "onCharResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<CharacterRes> call, Throwable t) {

            }
        });
    }

    private void addNewCharacter(Response<CharacterRes> response, String houseUrl) {
        CharacterModel model = new CharacterModel();

        model.setName(response.body().getName());
        model.setUrl(response.body().getUrl());
        model.setBorn(response.body().getBorn());
        model.setDied(response.body().getDied());
        model.setFather(response.body().getFather());
        model.setMother(response.body().getMother());
        model.setAllegiance(houseUrl);

        List<String> seasons = response.body().getTvSeries();
        if (seasons.size() > 0)
            model.setLastSeason(seasons.get(seasons.size() - 1));

        String aliases = "";

        for (String alias : response.body().getAliases()) {
            aliases = aliases + alias + "\n";
        }
        model.setAliases(aliases);

        String titles = "";
        for (String title : response.body().getTitles()) {
            titles = titles + title + "\n";
        }
        model.setTitles(titles);

        mAsyncSession.insertOrReplaceInTx(CharacterModel.class, model);
    }

    public List<List<CharacterModel>> getCharactersList() {
        List<List<CharacterModel>> lists = new ArrayList<>();
        String[] urls = {ConstantManager.STARK_HOUSE_URL,
                ConstantManager.LANNISTER_HOUSE_URL,
                ConstantManager.TARGARYEN_HOUSE_URL};

        for (String url : urls) {
            List<CharacterModel> list = mDaoSession
                    .queryBuilder(CharacterModel.class)
                    .where(CharacterModelDao.Properties.Allegiance.eq(url))
                    .build()
                    .list();
            Log.d(TAG, "getCharactersList: " + list);
            lists.add(list);
        }
        return lists;
    }

    public CharacterModel getCharacter(String url) {
        CharacterModel model = mDaoSession
                .queryBuilder(CharacterModel.class)
                .where(CharacterModelDao.Properties.Url.eq(url))
                .build()
                .unique();
        Log.d(TAG, "getCharacter: " + model);
        return model;
    }
}
