package com.hamom.romanzotov.data.network;

import com.hamom.romanzotov.data.network.res.CharacterRes;
import com.hamom.romanzotov.data.network.res.HouseRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestService {

    @GET("houses/{houseNo}")
    Call<HouseRes> houseReq(@Path("houseNo") String houseNo);

    @GET("characters/{characterNo}")
    Call<CharacterRes> characterReq(@Path("characterNo") String characterNo);

}
