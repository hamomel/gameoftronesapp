package com.hamom.romanzotov.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.stetho.Stetho;
import com.hamom.romanzotov.data.models.DaoMaster;
import com.hamom.romanzotov.data.models.DaoSession;

import org.greenrobot.greendao.database.Database;

public class GotApplication extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;
    private static SharedPreferences sSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "romanzotov-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    public static Context getContext() {
        return sContext;
    }
}
