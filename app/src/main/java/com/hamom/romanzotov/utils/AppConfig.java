package com.hamom.romanzotov.utils;

public interface AppConfig {
    String BASE_URL = "http://www.anapioficeandfire.com/api/";
    long MAX_CONNECT_TIMEOUT = 10000;
    long MAX_READ_TIMEOUT = 10000;
}
