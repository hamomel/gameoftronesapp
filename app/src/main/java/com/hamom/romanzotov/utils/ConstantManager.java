package com.hamom.romanzotov.utils;

public interface ConstantManager {

    String TAG_PREFIX = "Zotov ";

    String LANNISTER_HOUSE_URL = "http://www.anapioficeandfire.com/api/houses/229";
    String STARK_HOUSE_URL = "http://www.anapioficeandfire.com/api/houses/362";
    String TARGARYEN_HOUSE_URL = "http://www.anapioficeandfire.com/api/houses/378";

    String URL_KEY = "URL_KEY";

    Long ACTIVITY_START_DELAY = 10000L;
}
